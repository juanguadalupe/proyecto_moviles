<?php
// Conexión a la base de datos
$servername = "localhost";
$username = "username";
$password = "password";
$dbname = "myDB";

$conn = new mysqli($servername, $username, $password, $dbname);

// Verificar la conexión
if ($conn->connect_error) {
    die("Conexión fallida: " . $conn->connect_error);
}

// Obtener los datos del formulario
$nombre = $_POST["nombre"];
$direccion = $_POST["direccion"];
$telefono = $_POST["telefono"];

// Insertar los datos en la tabla "sucursales"
$sql = "INSERT INTO sucursales (nombre, direccion, telefono) VALUES ('$nombre', '$direccion', '$telefono')";

if ($conn->query($sql) === TRUE) {
    echo "Sucursal agregada exitosamente";
} else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}

$conn->close();
?>
